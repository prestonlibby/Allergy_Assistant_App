package com.appipen.interfaces;

import java.util.List;

import com.appipen.models.Store;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

/**
 * Created by Maddie on 11/12/2016.
 */

public interface WebService {

    //http://uvm.syncby.com/waypoints/near/?lon=-73.17511&lat=44.46805&radius=2
    @GET("stores")
    Call<List<Store>> AllStores();
    //

    //Call<Result>all();

    public static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://uvm.syncby.com//")
            //.baseUrl("http://uvm.syncby.com/?format=json/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();
}
