package com.appipen.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import com.appipen.activities.R;

public class LandingPage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing_page);
    }
    /*
        Brings you to the sign up page, SignUp.java and activity_sign_up.xml
     */
    public void signUpPage(View view) {
        Intent intent = new Intent(LandingPage.this, SignUp.class);
        startActivity(intent);
    }

    /*
        brings you directly to the map, will have authentication
     */
    public void signIn(View view) {
        Intent intent = new Intent(LandingPage.this, MapsActivity.class);
        startActivity(intent);
    }

}
