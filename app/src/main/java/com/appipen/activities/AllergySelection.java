package com.appipen.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import com.appipen.activities.R;

public class AllergySelection extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_allergy_selection);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //setting up checkbox and button objects
        final CheckBox peanut = (CheckBox) findViewById(R.id.peanutCheck);
        final CheckBox treeNut = (CheckBox) findViewById(R.id.treeCheck);
        final CheckBox milk = (CheckBox) findViewById(R.id.milkCheck);
        final CheckBox egg = (CheckBox) findViewById(R.id.eggCheck);
        final CheckBox gluten = (CheckBox) findViewById(R.id.glutenCheck);
        final CheckBox soy = (CheckBox) findViewById(R.id.soyCheck);
        final CheckBox sesame = (CheckBox) findViewById(R.id.sesameCheck);
        final Button submit = (Button) findViewById(R.id.submitAllergies);


        /* removing parts pertaining to floating action button
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        */
    }

    /*
        moves to map, will set values in database
     */
    public void submit(View view) {
        Intent intent = new Intent(AllergySelection.this, MapsActivity.class);
        startActivity(intent);
    }

}
