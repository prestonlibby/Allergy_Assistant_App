package com.appipen.models;

//import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//@Generated("org.jsonschema2pojo")
public class Waypoint {

    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("store")
    @Expose
    private Store store;
    @SerializedName("place")
    @Expose
    private String place;
    @SerializedName("location")
    @Expose
    private String location;

    /**
     *
     * @return
     * The url
     */
    public String getUrl() {
        return url;
    }

    /**
     *
     * @param url
     * The url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     *
     * @return
     * The store
     */
    public Store getStore() {
        return store;
    }

    /**
     *
     * @param store
     * The store
     */
    public void setStore(Store store) {
        this.store = store;
    }

    /**
     *
     * @return
     * The place
     */
    public String getPlace() {
        return place;
    }

    /**
     *
     * @param place
     * The place
     */
    public void setPlace(String place) {
        this.place = place;
    }

    /**
     *
     * @return
     * The location
     */
    public String getLocation() {
        return location;
    }

    /**
     *
     * @param location
     * The location
     */
    public void setLocation(String location) {
        this.location = location;
    }

}

