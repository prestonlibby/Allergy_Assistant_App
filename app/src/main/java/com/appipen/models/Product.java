package com.appipen.models;

import java.util.ArrayList;
import java.util.List;
//import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//@Generated("org.jsonschema2pojo")
public class Product {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("allergens")
    @Expose
    private List<Object> allergens = new ArrayList<Object>();

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The allergens
     */
    public List<Object> getAllergens() {
        return allergens;
    }

    /**
     *
     * @param allergens
     * The allergens
     */
    public void setAllergens(List<Object> allergens) {
        this.allergens = allergens;
    }

}