package com.appipen.models;

import java.util.ArrayList;
import java.util.List;
//import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//@Generated("org.jsonschema2pojo")
public class Store {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("waypoints")
    @Expose
    private List<Waypoint> waypoints = new ArrayList<Waypoint>();
    @SerializedName("products")
    @Expose
    private List<Product> products = new ArrayList<Product>();




    @Override
    public String toString() {
        return name;
    }



    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The waypoints
     */
    public List<Waypoint> getWaypoints() {
        return waypoints;
    }

    /**
     *
     * @param waypoints
     * The waypoints
     */
    public void setWaypoints(List<Waypoint> waypoints) {
        this.waypoints = waypoints;
    }

    /**
     *
     * @return
     * The products
     */
    public List<Product> getProducts() {
        return products;
    }

    /**
     *
     * @param products
     * The products
     */
    public void setProducts(List<Product> products) {
        this.products = products;
    }

}