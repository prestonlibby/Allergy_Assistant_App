# Allergy Assistant App

## Android App

(placeholder)

## REST-API webapp

More info is available in the [webapp docs](webapp/README.md)

## Development Tools

### IDE

[IntelliJ IDEA Ultimate](https://www.jetbrains.com/student/) (free for students)

Android Support (IntelliJ plugin - equivalent to Android Studio)

Python (IntelliJ plugin - equivalent to PyCharm)

### README.md/Markdown Editor

[Typora](http://typora.io) (Windows/Mac/Linux)

Markdown Support (IntelliJ plugin from JetBrains)

Markdown Navigator (IntelliJ plugin - free/paid)