from django.contrib.auth.models import User
from django.contrib.gis.db import models
from djplaces.fields import LocationField


class Store(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return '%s' % (self.name)


class Waypoint(models.Model):
    store = models.ForeignKey(Store, on_delete=models.CASCADE, related_name="waypoints")
    point = models.PointField(srid=4326, blank=True, null=True)

    # dj-places
    place = models.CharField(max_length=250)
    location = LocationField(base_field='place')

    objects = models.GeoManager()

    def __str__(self):
        return '%s (%s)' % (self.place, self.location)


class Allergen(models.Model):
    name = models.CharField(max_length=200)
    users = models.ManyToManyField(User, related_name='allergies',blank=True)

    def __str__(self):
        return '%s' % (self.name)


class Product(models.Model):
    name = models.CharField(max_length=200)
    store = models.ForeignKey(Store, on_delete=models.CASCADE, related_name='products')
    allergens = models.ManyToManyField(Allergen, related_name='products', through='Containing')

    def __str__(self):
        return '%s' % (self.name)


class Containing(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    allergen = models.ForeignKey(Allergen, on_delete=models.CASCADE)

    class Meta:
        #app_label = ''
        db_table = 'containing'


class Person(User):
    # Adds functionality without changing base table for user authentication
    pass

    class Meta:
        proxy = True
