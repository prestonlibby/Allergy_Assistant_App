import django_filters
from django.contrib.gis.geos import Point
from django.contrib.gis.measure import D
from rest_framework import viewsets
# User and group objects not necessary for app
from django.contrib.auth.models import User, Group
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.response import Response
from rest_framework import status

from .serializers import UserSerializer, GroupSerializer
# Our data model objects
from .models import Store, Product, Allergen, Waypoint
from .serializers import StoreSerializer, ProductSerializer, AllergenSerializer, WaypointSerializer
from rest_framework.decorators import detail_route, list_route


# class WaypointFilter(django_filters.rest_framework.FilterSet):
#     min_lon = django_filters.NumberFilter(name="lon", lookup_expr='gte')
#     max_lon = django_filters.NumberFilter(name="lon", lookup_expr='lte')
#
#     class Meta:
#         model = Waypoint
#         fields = ['min_lon', 'max_lon']


class StoreViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows stores to be viewed.
    """
    queryset = Store.objects.all()
    serializer_class = StoreSerializer


class WaypointViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows waypoints to be viewed.
    """
    queryset = Waypoint.objects.all()
    serializer_class = WaypointSerializer

    @list_route()
    def near(self, request):
        queryset = Waypoint.objects.all()
        lat = self.request.query_params.get('lat', None)
        lon = self.request.query_params.get('lon', None)
        radius = self.request.query_params.get('radius', None)

        # lat = 44.4680596
        # lon = -73.17511279999997

        try:
            # x first, then y
            origin = Point(float(lon), float(lat), srid=4326)
            d = float(radius)
            queryset = Waypoint.objects.filter(point__distance_lte=(origin, D(mi=d)))
        except:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class ProductViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows products to be viewed.
    """
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class AllergenViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows allergens to be viewed.
    """
    queryset = Allergen.objects.all()
    serializer_class = AllergenSerializer


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows users to be viewed by admins.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAdminUser,)

    @detail_route(methods=['get'], permission_classes=[IsAuthenticated])
    def allergies(self, request, pk=None):
        user_allergies = Allergen.objects.filter(users__allergies__users=pk)
        serializer = AllergenSerializer(user_allergies, many=True)

        return Response(serializer.data)


# Not necessary for app
class GroupViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows groups to be viewed by admins.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = (IsAdminUser,)