from django.contrib.auth.models import User, Group
from .models import Store, Product, Allergen, Waypoint
from rest_framework import serializers


class UserAllergiesSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Allergen
        fields = ('name',)


class UserSerializer(serializers.HyperlinkedModelSerializer):
    allergies = UserAllergiesSerializer(many=True)

    class Meta:
        model = User
        fields = ('url', 'username', 'allergies')


class GroupSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Group
        fields = ('url', 'name')


class StoreProductsSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Product
        fields = ('name',)


class WaypointStoreSerializer(serializers.HyperlinkedModelSerializer):
    products = StoreProductsSerializer(many=True)

    class Meta:
        model = Store
        fields = ('name', 'products')


class WaypointSerializer(serializers.HyperlinkedModelSerializer):
    store = WaypointStoreSerializer()

    class Meta:
        model = Waypoint
        fields = ('store', 'place', 'location')


class AllergenSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Allergen
        fields = ('name',)


class ProductSerializer(serializers.HyperlinkedModelSerializer):
    allergens = AllergenSerializer(many=True)

    class Meta:
        model = Product
        fields = ('name', 'allergens')


class StoreSerializer(serializers.HyperlinkedModelSerializer):
    waypoints = WaypointSerializer(many=True)
    products = StoreProductsSerializer(many=True)

    class Meta:
        model = Store
        fields = ('name', 'waypoints', 'products')
