from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User
from django.contrib.gis import admin, forms
from django.contrib.gis.geos import Point, fromstr

from restapi.forms import WaypointForm
from .models import Store, Product, Allergen, Waypoint
import nested_admin


class WaypointAdminForm(forms.ModelForm):
    point = forms.PointField(widget=forms.OSMWidget(attrs={
        'display_raw': True}))


class WaypointAdmin(admin.ModelAdmin):
    model = Waypoint
    form = WaypointForm
    fields = ('store', 'place', 'longitude', 'latitude', 'location',)
    list_display = ('store', 'place', 'point')

    def save_model(self, request, obj, form, change):
        if obj.location:
            s = str(obj.location).split(',')
            f = (float(s[1]),float(s[0]))
            obj.point = Point(f, srid=4326)
        obj.save()


class WaypointInline(admin.TabularInline):
    model = Waypoint
    readonly_fields = ('place', 'location',)


class ProductAllergensInline(nested_admin.NestedStackedInline):
    model = Product.allergens.through
    extra = 0


class AllergenInline(admin.StackedInline):
    model = Allergen


class ProductInline(nested_admin.NestedStackedInline):
    model = Product
    inlines = [
        ProductAllergensInline,
    ]
    extra = 0


class ProductAdmin(admin.ModelAdmin):
    model = Product

    inlines = [
        ProductAllergensInline,
    ]


class AllergenAdmin(admin.ModelAdmin):
    exclude = ('products','users')


class StoreAdmin(nested_admin.NestedModelAdmin):
    inlines = [
        ProductInline,
        WaypointInline,
    ]


class UserAllergiesInline(admin.StackedInline):
    model = Allergen.users.through
    extra = 3
    verbose_name = 'Allergy'
    verbose_name_plural = 'Allergies'
    classes = ('grp-collapse grp-open',)
    inline_classes = ('grp-collapse grp-open',)

# Define a new User admin
class UserAdmin(BaseUserAdmin):
    list_display = ('username', 'is_staff', 'is_superuser')
    inlines = (UserAllergiesInline, )
    inline_classes = ('grp-collapse grp-open',)

    fieldsets = (
        ('', {
            'fields': ('username',),
        }),
        ('Permissions', {
            'classes': ('grp-collapse grp-closed',),
            'fields' : ('password', 'is_active', 'is_staff', 'is_superuser',),
        }),
    )

# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)

admin.site.register(Store, StoreAdmin)
admin.site.register(Waypoint, WaypointAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Allergen, AllergenAdmin)
