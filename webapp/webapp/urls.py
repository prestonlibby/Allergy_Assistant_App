"""URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from rest_framework import routers
from rest_framework.schemas import get_schema_view

from restapi import views
from webapp import settings

admin.autodiscover()

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)
router.register(r'stores', views.StoreViewSet)
router.register(r'waypoints', views.WaypointViewSet)
router.register(r'products', views.ProductViewSet)
router.register(r'allergens', views.AllergenViewSet)

schema_view = get_schema_view(title='Rest API')

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url('^schema/$', schema_view),
    url('^', include(router.urls)),
    url('^admin/', include(admin.site.urls)),
    url('^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url('^_nested_admin/', include('nested_admin.urls')),
    url(r'^grappelli/', include('grappelli.urls')), # grappelli URLS
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]