## Allergy Assistant REST-API webapp

## Introduction

The REST-API uses [Django](djangoproject.org) and [Django Rest Framework](django-rest-framework.org) (DRF) to quickly model data, create the schema, and expose it through views as json on a web server.

## Folder Structure

```shell
webapp					Django project root and IDEA module within the main Android project
├── db.sqlite3			SQLite database
├── manage.py			Runs all Django commands
├── README.md			README file for the webapp
├── requirements.txt	List of dependencies for automatically install in a virtualenv with pip
├── restapi				Django app folder
│   ├── admin.py		The admin website model registry
│   ├── models.py		The models in the object model and schema
│   ├── serializers.py	The REST framework serializers convert the object to json
│   ├── tests.py		
│   ├── views.py		In our case these are equivalent to database queries
├── static				All of the static css and js files (for /admin) are collected here
├── templates			Templates would be here for a website, but probably not for ours
├── webapp				The Django project named webapp
    ├── settings
    │   ├── base.py		Common settings for both development and production
    │   ├── dev.py		Development settings
    │   ├── prod.py		Production settings
    ├── urls.py			URL router similar to .htaccess rewrites
    ├── wsgi.py			Required entry point for the web server that is run first
```

## Installation

### Prerequisites

- [Python 2.x](https://www.python.org/downloads/) (UVM Silk uses 2.6.6 but 2.7 should be fine)
- [IntelliJ IDEA](https://www.jetbrains.com/student/) or PyCharm if you want an IDE that can do Python
- The remaining requirements can be installed using the python package manager: pip.

### Configuring Python Development Environment

#### Windows

Open a command prompt and check the python version

```powershell
python --version
```

If the version is 2.x you could instead just use pip install. Otherwise preface the path to 2.x:

```powershell
C:\Python27\python.exe -m pip install virtualenv
cd %USERPROFILE%\IdeaProjects\Allergy_Assistant_App\webapp
virtualenv -p C:\Python27\python.exe .venv
```

That creates a python environment within the folder .venv. Now activate it for this session:

```powershell
.venv\Scripts\activate
```

#### macOS / Linux

Open terminal and check the python version

```sh
python --version
python3 --version
/usr/bin/python --version
/usr/bin/python2.7 --version
/opt/local/bin/python2.7 --version
```

When you've found the right 2.x version of python, use that full path in the following lines:

```sh
/usr/bin/python2.7 -m pip install virtualenv
cd ~/IdeaProjects/Allergy_Assistant_App/webapp
virtualenv -p /usr/bin/python2.7 .venv
```

That creates a python environment within the directory .venv. Now activate it for this session:

```sh
cd ~/IdeaProjects/Allergy_Assistant_App/webapp
source .venv/bin/activate
```

#### Within the virtualenv is a self-contained Python 🐍

After activating the virtualenv and while the command prompt is still open, you can run Python 2 directly:

```sh
python --version
pip list
```

Note that the production web server also runs in the same environment, also using Python 2.

### Install the required Python modules

Automatically install the project's dependencies in the virtualenv, such as django and djangorestframework.

```
pip install -r requirements.txt
pip list
```

### Sync the models to the local database

The data models must be synced to the database once to setup the local DB, and again when changes are made to models.py. Changes to other files such as views.py would not require the syncdb command.

```sh
python manage.py syncdb
```

This runs all the CREATE TABLE and ALTER TABLE sql commands to make the DB match the code.

### Test the app on the built-in development server

Start the web server by using the runserver command. It will not return that prompt or stop until you press Ctrl-C.

```
python manage.py runserver
```

View the self-documenting API at http://localhost:8000 while the web server is running.

Congratulations, you now have a mini web server and database system locally on your computer. The production server can have a different Apache web server and a different database system without much changing.

## Troubleshooting

If you have trouble running the webserver, you can try this instead:

```
python manage.py runserver --settings=webapp.settings.dev
```

That option can be added to other commands to ensure you're using development settings, but should not typically be necessary.